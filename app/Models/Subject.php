<?php

namespace App\Models;
use App\Model\User;
class Subject
{
    protected $table = 'subject';
    public $timestamps = true;
    protected $fillable = [
        'name'
    ];
    /**
     * The subjects that belong to the User.
     */
    public function assignedSubject()
    {
        return $this->belongsToMany(User::class, 'subject_menu', 'subject_id', 'user_id');
    }
}
