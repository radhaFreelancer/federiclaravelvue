<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Model\Subject;

class subjects extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "id" => 1,
                "name" => "Java",
                "created_at" => Carbon::now()
            ],
            [
                "id" => 2,
                "name" => "PHP",
                "created_at" => Carbon::now()
            ],
        ];

        Subject::insert($data);
    }
}
