<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Model\User;

class user_subject extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /**
         * superadmin => 1
         * admin => 2
         */
        $assignedSubjects = array(
            1=>array(1,2),
            2=>array(1),
        );

        foreach($assignedMenus as $user=>$subject_ids)
        {
            $item = User::findOrFail($user);
            $item->assignedSubject()->sync($subject_ids);
        }
    }
}
