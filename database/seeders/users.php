<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Model\User;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "admin",
                "email" => "admin@gmail.com",
                "password" => Hash::make('admin!@#'),
                "created_at" => Carbon::now()
            ],
            [
                "name" => "Superadmin",
                "email" => "superadmin@gmail.com",
                "password" => Hash::make('admin!@#'),
                "created_at" => Carbon::now()
            ],
        ];

        User::insert($data);
    }
}
