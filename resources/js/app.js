require('./bootstrap');

import * as Vue from 'vue';
// import Vue from 'vue/dist/vue.common.js';
import App from './vue/App.vue';

const app = new Vue({
    el:'#app',
    component:{ App }
})